import { lazy } from "react";

import SignIn from "../pages/SignIn";

 const SignUp = lazy(() => import('../pages/SignUp'))
 const DashboardLayout = lazy(() => import('../pages/Dashboard'))

export const routes = [
    //Modulo SignIn
    {
        to: '/sign-in/',
        path: '/sign-in/',
        Component: SignIn, 
    },
    //Modulo SignUp
    {
        to: '/sign-up',
        path: '/sign-up',
        Component: SignUp, 
    },
     //Modulo Admin
     {
        to: '/admin',
        path: '/admin',
        Component: DashboardLayout, 
    },
]

export default routes