import { CircularProgress } from '@mui/material';
import { Suspense, useEffect } from 'react';
import {
    Route,
    Routes,
    BrowserRouter,
    Navigate
} from 'react-router-dom';
import { routes } from './routes.ts';
import { useDispatch } from 'react-redux';
import { firebase } from '../firebase/firebaseConfig'
import { login } from '../actions/auth';
import { uiLogin } from '../actions/uiLogged';


export const Navigation = () => {

    const dispatch = useDispatch()

    useEffect(() => {

        firebase.auth().onAuthStateChanged((user) => {
            //aca pregunto si el usuerio ya esta o estuvo utenticado
            if (user?.uid) {
                dispatch(login(user.uid, user.displayName))
                dispatch(uiLogin(login))
            }
        }
        )
    }, [dispatch])

    return (

        <Suspense fallback={<CircularProgress color="success" />}>
            <BrowserRouter>
                <div className="main-layout">
                    <Routes>
                        {
                            routes.map(({ to, path, Component }) =>
                                <Route key={to} path={path} element={<Component />} />
                            )
                        }
                        <Route path="/*" element={<Navigate to={routes[0].to} />} />
                    </Routes>
                </div>
            </BrowserRouter>
        </Suspense>
    );
}
