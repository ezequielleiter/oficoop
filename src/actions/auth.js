
import { types } from "../types/types";
import { firebase, googleAuthProvider } from '../firebase/firebaseConfig'
import Swal from "sweetalert2";

//aca hago una accion con una peticion asincrona para el login
export const startLoginEmailPassword = (email, password) => {
    // es una funcion que retorna un callbak
    // el dispatch que pongo aca despues se hacer cargo redux-tunk
    return (dispatch) => {

        // esta accion la uso para habilitar o desabilitar el boton de sing in mienstras ser carga
        // dispatch(startLoading())
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(({ user }) => {
                dispatch(login(user.uid, user.displayName))
                //aca uso la misma para el cuando se termina de logear
                // dispatch(finishLoading())
            })
            .catch(e => {
                if (e.code === 'auth/user-not-found') {
                    Swal.fire({
                        icon: 'error',
                        title: 'Algo salio mal...',
                        text: '¡No estas registrado!',
                        footer: '<a href="/sing-up">Registrate aca</a>'
                    })
                } else if (e.code === 'auth/wrong-password') {
                    Swal.fire({
                        icon: 'error',
                        title: 'Algo salio mal...',
                        text: '¡Pusiste mal la contraseña!',
                    })
                }
            })
    }
}

//registrar en firebase con datos del formulario
export const startRegisterWhithEmailPasswordName = (email, password, firstName) => {
    return (dispatch) => {
        firebase.auth().createUserWithEmailAndPassword(email, password, firstName)
            .then(async ({ user }) => {
                await user.updateProfile({ displayName: firstName })

                dispatch(login(user.uid, user.displayName))
            }).catch(e => {
                console.log(e)
            })
    }
}

//esta es la autenticacion con google
//todo este codigo que tengo aca va a retornar una promesa
export const startGoogleLogin = () => {
    return (dispatch) => {
        firebase.auth().signInWithPopup(googleAuthProvider)
            .then(({ user }) => {
                dispatch(login(user.uid, user.displayName))
            })
    }
}

export const login = (uid, displayName, login) => (
    {
        type: types.login,
        payload: {
            uid,
            displayName,
            login: true
        }
    }
)

//logout de firebase
export const startLogout = () => {
    return async (dispatch) => {
        await firebase.auth().signOut();
        dispatch(logout())
    }

}


export const logout = () => (
    {
        type: types.logout,
        payload: {
            uid: '',
            name: '',
            login: false
        }
    }
)