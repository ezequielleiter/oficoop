import { types } from "../types/types";

export const uiLogin = () => (
    {
        type: types.uiLogin,
        payload: {}
    }
)

export const uiLogout = () => (
    {
        type: types.uiLogout,
        payload: {}
    }
)


export const startLoading = () => (
    {
        type: types.uiStartLoading
    }
)


export const finishLoading = () => (
    {
        type: types.uiFinishLoading
    }
)
