import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk';
import { authReducer } from '../auth/authReducer'
import { uiReducer } from '../reducers/uiReducer';

// con esta const puedo usar redux y a la vez el middleware de redux-tuck para hacer peticiones asincronas
const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

//con esto puedo combinar reducer y si tengo una funcionalidad nueva
// la puedo sumar aca   
const reducers = combineReducers({
    auth: authReducer,
    ui: uiReducer
})

//asi solo recibe un solo reducer
export const store = createStore(reducers,
    //asi se configura la peticion asincrona con el middleware
    composeEnhancers(
        applyMiddleware(thunk)
    )
)