//aca tengo el tipo de login y logout de la app

export const types = {
    login: '[auth] Login',
    logout: '[auth] Logout',

    uiLogin: '[UI] Login',
    uiLogout: '[UI] Logout',

    uiStartLoading: '[UI] Start loading',
    uiFinishLoading: '[UI] Finish loading'
};
