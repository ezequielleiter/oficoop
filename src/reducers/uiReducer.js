import { types } from '../types/types';

const initialState = {
    login: false,
    loading: false
}

export const uiReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.uiLogin:
            return {
                ...state,
                login: true
            }
        case types.uiLogout:
            return {
                ...state,
                login: false
            }

        case types.uiStartLoading:
            return {
                ...state,
                loading: true
            }
        case types.uiFinishLoading:
            return {
                ...state,
                loading: false
            }

        default:
            return state
    }

}