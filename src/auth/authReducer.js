import { types } from '../types/types';

const authInitialState = {
  uid: '',
  displayName: '',
  login: false
}


export const authReducer = (state = authInitialState, action) => {
  switch (action.type) {
    case types.login:
      return {
        uid: action.payload.uid,
        displayName: action.payload.displayName,
        login: true
      };
    case types.logout:
      return {};
    default:
      return state;
  }
};