import { useContext } from "react"
import { Navigate } from "react-router-dom"
import { AuthContext } from "../auth/authContext"

export const AdminLayout = ({ children }) => {

    const { user } = useContext(AuthContext);
    // //aca pregunto si el usuario esta autenticado
    // // y si lo esta que renderice los children
    return user.logged ? children : <p>algo salio mal</p>
    // return <p>hola mundo</p>
}


export default AdminLayout