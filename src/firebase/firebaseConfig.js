import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { GoogleAuthProvider } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyAQKi2lNrMJbyPXpUhRwy4yUf32YnOo8ms",
    authDomain: "oficoop-59d44.firebaseapp.com",
    projectId: "oficoop-59d44",
    storageBucket: "oficoop-59d44.appspot.com",
    messagingSenderId: "153186614167",
    appId: "1:153186614167:web:addc27b8cdf4c0cb6322d2"
};

// esta es la base de datos
const app = firebase.initializeApp(firebaseConfig);


// esta es la referencia a firestore
const db = firebase.firestore();
// y este es mi authprovider para google, si fuera con otro es igual
const googleAuthProvider = new GoogleAuthProvider()


export {
    app,
    db,
    googleAuthProvider,
    firebase,
}