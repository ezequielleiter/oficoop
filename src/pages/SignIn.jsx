import * as React from 'react';
import * as Yup from 'yup';
import Avatar from '@mui/material/Avatar';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import LoadingButton from '@mui/lab/LoadingButton';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { NavLink, useNavigate } from 'react-router-dom';
import { Navigate } from "react-router-dom"
import { useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { startGoogleLogin, startLoginEmailPassword } from '../actions/auth';
import GoogleIcon from '@mui/icons-material/Google';
import { IconButton } from '@mui/material';


const theme = createTheme();

export default function SignIn() {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    //asi saco la informacion del state
    const { loading } = useSelector(state => state.ui)
    const { login } = useSelector(state => state.auth)



    const handleLogin = (value) => {
        dispatch(startLoginEmailPassword(value.email, value.password))
        navigate('/admin')
    }

    // esta es la accion de autenticacion con google
    const hanbleGoogleLogin = () => {
        dispatch(startGoogleLogin())
    }

    const { handleSubmit, handleChange, errors } = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        onSubmit: handleLogin,
        validationSchema: Yup.object({
            email: Yup.string().email('creo que pusiste mal tu mail')
                .required('Tu email es requerido'),
            password: Yup.string()
                .required('La contraseña es requerida')
                .max(8, 'contraseña demasiado larga ')
                .min(4, 'contraseña demasiado corta'),
        })
    })



    return (
        login === true ? <Navigate to='/admin' /> :
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '100vh' }}>
                <CssBaseline />
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={7}
                    sx={{
                        backgroundImage: 'url(https://source.unsplash.com/random)',
                        backgroundRepeat: 'no-repeat',
                        backgroundColor: (t) =>
                            t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                        backgroundSize: 'cover',
                        backgroundPosition: 'center',
                    }}
                />
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <Box
                        sx={{
                            my: 8,
                            mx: 4,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Ofi-Coop
                        </Typography>

                        <Box component="form" noValidate
                                onSubmit={handleSubmit} 
                            sx={{ mt: 1 }}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                autoFocus
                                onChange={handleChange}
                                error={errors.email && Boolean(errors.email)}
                                helperText={Boolean(errors.email) && errors.email}
                            />
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                onChange={handleChange}
                                error={errors.password && Boolean(errors.password)}
                                helperText={Boolean(errors.password) && errors.password}
                            />
                            {/* <FormControlLabel
                                control={<Checkbox value="remember" color="primary" />}
                                label="Recordarme"
                            /> */}
                                <LoadingButton
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb: 2 }}
                                    loading={loading}

                            >
                                Sign In
                                </LoadingButton>
                                <Typography>Ingresar con:</Typography>
                                <IconButton size="large" onClick={hanbleGoogleLogin}>
                                    <GoogleIcon fontSize="inherit" />
                                </IconButton>
                            <Grid container>
                                <Grid item xs>
                                    <Link href="#" variant="body2">
                                        Forgot password?
                                    </Link>
                                </Grid>
                                <Grid item>
                                        <Link href="#" variant="body2">
                                    </Link>
                                    <NavLink to="/sign-up">{"No estas registrado? ¡Registrate aca!"}</NavLink>
                                </Grid>
                            </Grid>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </ThemeProvider> 
    )
}
