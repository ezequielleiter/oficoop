import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { startRegisterWhithEmailPasswordName } from '../actions/auth';
import { useDispatch } from 'react-redux';

const theme = createTheme();
const FormikContext = React.createContext({});

export default function SignUp() {
    //sin YUP
    // const validate = (values) => {

    //     const errors = {};

    //     if (!values.firstName) {
    //         errors.firstName = 'Tu nombre es requerido';
    //     }

    //     if (!values.lastName) {
    //         errors.lastName = 'Tu apellido es requerido';
    //     }

    //     if (!values.email) {
    //         errors.email = 'Tu email es requerido';
    //     } else if (
    //         !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
    //     ) {
    //         errors.email = 'creo que pusiste mal tu mail';
    //     }

    //     if (!values.password) {
    //         errors.password = 'Required';
    //     } else if (values.password <= 8) {
    //         errors.password = 'contraseña demasiado corta';
    //     }

    //     return errors;
    // }
    const dispatch = useDispatch();

    const handleRegister = (value) => {
        dispatch(startRegisterWhithEmailPasswordName(value.email, value.password, value.firstName))
    }


    const { handleSubmit, handleChange, errors, formikStateAndHelpers } = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            cooperativa: '',
            email: '',
            password: '',
            passwordConfirm: '',

        },
        onSubmit: handleRegister,
        validationSchema: Yup.object({
            firstName: Yup.string()
                .required('Tu nombre es requerido'),
            lastName: Yup.string()
                .required('Tu apellido es requerido'),
            cooperativa: Yup.string()
                .required('Tu cooperativa es requerida'),
            email: Yup.string().email('creo que pusiste mal tu mail')
                .required('Tu email es requerido'),
            password: Yup.string()
                .required('La contraseña es requerida')
                .max(8, 'contraseña demasiado larga ')
                .min(4, 'contraseña demasiado corta'),
            // Confirmacion de constraseña
            passwordConfirm: Yup.string()
                .oneOf([Yup.ref('password'), null], 'Passwords must match')
        })
    })

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign up
                    </Typography>
                    <FormikContext.Provider value={formikStateAndHelpers}>
                        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>

                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        autoComplete="given-name"
                                        name="firstName"
                                        required
                                        fullWidth
                                        id="firstName"
                                        label="Nombre"
                                        autoFocus
                                        onChange={handleChange}
                                        error={errors.firstName && Boolean(errors.firstName)}
                                        helperText={Boolean(errors.firstName) && errors.firstName}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        required
                                        fullWidth
                                        id="lastName"
                                        label="Apellido"
                                        name="lastName"
                                        autoComplete="family-name"
                                        onChange={handleChange}
                                        error={errors.lastName && Boolean(errors.lastName)}
                                        helperText={Boolean(errors.lastName) && errors.lastName}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        required
                                        fullWidth
                                        id="cooperativa"
                                        label="Cooperativa"
                                        name="cooperativa"
                                        onChange={handleChange}
                                        error={errors.lastName && Boolean(errors.lastName)}
                                        helperText={Boolean(errors.lastName) && errors.lastName}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        required
                                        fullWidth
                                        id="email"
                                        label="Email Address"
                                        name="email"
                                        autoComplete="email"
                                        onChange={handleChange}
                                        error={errors.email && Boolean(errors.email)}
                                        helperText={Boolean(errors.email) && errors.email}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        required
                                        fullWidth
                                        name="password"
                                        label="Password"
                                        type="password"
                                        id="password"
                                        autoComplete="new-password"
                                        onChange={handleChange}
                                        error={errors.password && Boolean(errors.password)}
                                        helperText={Boolean(errors.password) && errors.password}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        required
                                        fullWidth
                                        name="passwordConfirm"
                                        label="Password Confirm"
                                        type="password"
                                        id="passwordConfirm"
                                        onChange={handleChange}
                                        error={errors.passwordConfirm && Boolean(errors.passwordConfirm)}
                                        helperText={Boolean(errors.passwordConfirm) && errors.passwordConfirm}
                                    />
                                </Grid>
                            </Grid>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb: 2 }}
                            >
                                Sign Up
                            </Button>
                            <Grid container justifyContent="flex-end">
                                <Grid item>
                                    <Link href="/sign-in" variant="body2">
                                        Ya estas registrado? ingrasa aca
                                    </Link>
                                </Grid>
                            </Grid>
                        </Box>
                    </FormikContext.Provider>
                </Box>
            </Container>
        </ThemeProvider >
    );
}
